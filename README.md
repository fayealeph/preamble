# preamble

LaTeX preambles for note taking and problem sets.

## Contents
### Preambles
* `preamble.tex` - preamble for note taking.
* `preamblePSet.tex` - preamble for PSets. has slightly modified theorem environments compared to `preamble.tex`.
### Scripts
* `compileAll.sh` - runs `compilePreamblePSet.sh` and `compilePreamble.sh`.
* `compilePreamblePSet.sh` - combines subfiles into `preamblePSet.tex`. Uses `preambleTheoremPSet.tex`.
* `compilePreamble.sh` - combines subfiles into `preamble.tex`. Uses `preambleTheorem.tex`.
* `watchChangesAndCompile.sh` - does what it says. only watches `preambleTheorem.tex` but that is easy to change.
### Examples
* `examples/` - contains templates/examples of usage.
