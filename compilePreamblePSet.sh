#!/bin/bash

cat preamblePackage.tex > preamblePSet.tex
cat preambleColor.tex >> preamblePSet.tex
cat preambleEToolBox.tex >> preamblePSet.tex
cat preambleTheoremPSet.tex >> preamblePSet.tex
cat preambleCommands.tex >> preamblePSet.tex
cat preambleFormat.tex >> preamblePSet.tex
